Name:           oxrightkey
Version:        0.1
Release:        2%{?dist}
Summary:        touch screen right click emulate
License:        GPL
Source0:        %{name}-%{version}.tar.gz 
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  automake
BuildRequires:	gcc,libX11-devel, libXtst-devel, libxml2-devel

%description
touch screen right click emulate
%prep
%setup -q

%build
autoreconf -v --install || exit 1
%configure
make %{?_smp_mflags} LIBS='-lX11 -lXtst' 

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}/etc/
install -m644 oxrightkey.conf %{buildroot}/etc/

%clean
rm -rf %{buildroot}

%files
%{_bindir}/%{name}
/etc/oxrightkey.conf

%changelog
* Tue Apr 23 2013 AlanTom <alan.tom@ossii.com.tw> ossii -0.1-2
- Add /etc/oxringtkey.conf list all touch device.
- Add contextmenukey() workaround nautilus and stardict.
- Add listxquery() avoid sugar-desktop use contextmenukey.
 
* Mon Mar 18 2013 AlanTom <alan.tom@ossii.com.tw> ossii -0.1-1
- first initial
